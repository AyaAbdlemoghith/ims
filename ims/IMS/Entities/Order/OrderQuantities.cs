using MongoDB.Bson;

namespace IMS.Entities;

public class OrderQuantities
{
    public ObjectId ProdId { get; set; }
    public int OrderQuantity { get; set; }
    public OrderQuantities(ObjectId prodId, int orderQuantity)
    {
        ProdId = prodId;
        OrderQuantity = orderQuantity;
    }
}