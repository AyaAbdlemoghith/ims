using MongoDB.Bson;
//using FalconFlexCommon.Mongo.BaseClasses;


namespace IMS.Entities;

public class Order //: BaseEntity
{
    public ObjectId OrderId { get; init; }
    public string OrderName { get; set; }
    public ObjectId WarehouseId { get; set; }
    public List<OrderQuantities> OrderQuantities { get; set; } 


    public Order(ObjectId orderId, string orderName, ObjectId warehouseId, List<OrderQuantities> orderQuantities)
    {
        OrderId = orderId;
        OrderName = orderName;
        WarehouseId = warehouseId;
        OrderQuantities = orderQuantities;
    }
}