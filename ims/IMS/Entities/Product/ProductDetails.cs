
using CSharpFunctionalExtensions;
namespace IMS.Entities;

public record ProductDetails//:ValueObject
{
    public decimal ProdPrice { get; init; }
    public DateTime ProdExpiryDate { get; init; }
    public string ProdCategory { get; init; }
}
//Hase table and Equality no need because it's record already