using MongoDB.Bson;

namespace IMS.Entities;

public class ProductQuantities
{
    public ObjectId WarehouseId { get; set; }
    public int ProdQuantity { get; set; }
    public int UsableQuantity { get; set; }
    
    public ProductQuantities(ObjectId warehouse, int prodQuantity, int usableQuantity)
    {
        WarehouseId = warehouse;
        ProdQuantity = prodQuantity;
        UsableQuantity = usableQuantity;
    }
}