
//using FalconFlexCommon.Mongo.BaseClasses;

using MongoDB.Bson;

namespace IMS.Entities;

public class Product//:BaseEntity
{
    
    public ObjectId ProdId { get; init; }  
    public string ProdName { get; set; }
    public ProductDetails Details { get; set; }//ValueObject

    public List<ProductQuantities> ProdQuantities { get; set; }

    public Product(ObjectId prodId, string prodName, ProductDetails details, List<ProductQuantities> prodQuantities)
    {
        ProdId = prodId;
        ProdName = prodName;
        Details = details;
        ProdQuantities = prodQuantities;
    }
}

