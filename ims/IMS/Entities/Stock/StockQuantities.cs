using MongoDB.Bson;

namespace IMS.Entities;

public class StockQuantities
{
    public ObjectId ProdId { get; set; }  // Reference to Warehouse
    public int StockQuantity { get; set; }
    
    public StockQuantities(ObjectId prodId, int stockQuantity)
    {
        ProdId = prodId;
        StockQuantity = stockQuantity;
    }
}