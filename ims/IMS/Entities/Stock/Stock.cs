using MongoDB.Bson;

namespace IMS.Entities;

public class Stock
{
    // Properties
    public ObjectId StockId { get; init; }
    public string StockName { get; set; }
    public ObjectId WarehouseId { get; set; }  // Reference to Warehouse
    public List<StockQuantities> StockQuantities { get; set; }  // List of quantities


    // Constructor
    public Stock(ObjectId stockId, string stockName, ObjectId warehouseId, List<StockQuantities> stockQuantities)
    {
        StockId = stockId;
        StockName = stockName;
        WarehouseId = warehouseId;
        StockQuantities = stockQuantities;
    }
}
