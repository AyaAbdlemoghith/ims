using MongoDB.Bson;

namespace IMS.Entities;

public class User
{
    public ObjectId UserId { get; init; }
    public string UserName { get; set; }

    public User(ObjectId userId, string userName)
    {
        UserId = userId;
        UserName = userName;
    }
    
}
