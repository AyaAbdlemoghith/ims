using MongoDB.Bson;

namespace IMS.Entities;

public class Warehouse
{
    public ObjectId WarehouseId { get; init; } //ObjectID
    public string WarehouseName { get; set; }
    public string WarehouseLocation { get; set; }

    public Warehouse(ObjectId warehouseId, string warehouseName, string warehouseLocation)
    {
        WarehouseId = warehouseId;
        WarehouseName = warehouseName;
        WarehouseLocation = warehouseLocation;
    }
}