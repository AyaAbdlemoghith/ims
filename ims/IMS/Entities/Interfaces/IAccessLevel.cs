namespace IMS.Entities.Interfaces;

// Define an interface for access level actions
public interface IAccessLevel
{
    void PerformAction();
}
