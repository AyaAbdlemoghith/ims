namespace IMS.Enums;

// Define an enum for access levels
public enum AccessLevel
{
    Employee,
    Admin
}