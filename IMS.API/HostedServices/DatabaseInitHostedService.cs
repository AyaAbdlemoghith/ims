using FalconFlexCommon.Mongo.DbContext;
using FalconFlexCommon.Mongo.Misc;
using FalconFlexCommon.Mongo.Repositories;
using IMS.Domain.Entities;
using IMS.Domain.Entities.Shipment;
using IMS.Domain.Entities.Product;
using IMS.Domain.Entities.Order;


namespace IMS.API.HostedServices;

public class DatabaseInitHostedService : IHostedService
{
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public DatabaseInitHostedService(IServiceScopeFactory serviceScopeFactory)
    {
        _serviceScopeFactory = serviceScopeFactory;
    }
    
    public async Task StartAsync(CancellationToken cancellationToken)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var databaseContext = scope.ServiceProvider.GetRequiredService<IDatabaseContext>();
                var warehouseRepository = scope.ServiceProvider.GetRequiredService<IRepository<Warehouse>>();
                var productRepository = scope.ServiceProvider.GetRequiredService<IRepository<Product>>();
                var orderRepository = scope.ServiceProvider.GetRequiredService<IRepository<Order>>();
                var shipmentRepository = scope.ServiceProvider.GetRequiredService<IRepository<Shipment>>();

                
                //Ensure Collections Exists
                databaseContext.EnsureCollectionExists(nameof(Warehouse));
                databaseContext.EnsureCollectionExists(nameof(Product));
                databaseContext.EnsureCollectionExists(nameof(Order));
                databaseContext.EnsureCollectionExists(nameof(Shipment));
                await databaseContext.CreateIndex(warehouseRepository,
                    OrderBuilder<Warehouse>.Create()
                        .Descending(w => w.WarehouseLocation)
                        .Descending(w => w.WarehouseName), "WarehouseLocationAndWarehouseNameIdx");
                
                await databaseContext.CreateIndex(productRepository,
                    OrderBuilder<Product>.Create()
                        .Descending(p => p.CategoryId)
                        .Descending(p => p.ProductPrice), "CategoryIdAndProdPriceIdx");
                
                await databaseContext.CreateIndex(productRepository,
                    OrderBuilder<Product>.Create()
                        .Descending(p => p.CategoryId)
                        .Descending(p => p.ProductPrice)
                        .Descending(p => p.ProductExpiryDate),"CategoryIdAndProdPriceAndExpiryDateIdx");
                
                await databaseContext.CreateIndex(orderRepository,
                    OrderBuilder<Order>.Create()
                        .Descending(o => o.OrderDate)
                        .Descending(o => o.WarehouseId), "OrderDateAndWarehouseIdx");

                await databaseContext.CreateIndex(shipmentRepository,
                    OrderBuilder<Shipment>.Create()
                        .Descending(s => s.ShipmentDate)
                        .Descending(s => s.WarehouseId), "ShipmentDateAndWarehouseIdx");
                
         
            }
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
}