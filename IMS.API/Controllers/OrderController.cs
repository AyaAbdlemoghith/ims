using System.Net;
using Microsoft.AspNetCore.Mvc;
using IMS.API.Extensions;
using IMS.API.DTOs.OrderDTOs;
using IMS.API.RequestModels.OrderReqModel;
using IMS.API.Services.Interfaces;
using Serilog;
using SnoonuCommons.Helpers;
namespace IMS.API.Controllers;
 
[ApiController]
[Route("api/v1/[controller]")]
public class OrderController : Controller
{
    private readonly IOrderService _orderService;

    public OrderController(IOrderService orderService)
    {
        _orderService = orderService;
    }

    [HttpPost]
    [ProducesResponseType(typeof(OrderDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<OrderDto>> CreateAsync([FromBody] CreateOrderRequest createOrderRequest)
    {
        try
        {
            var order = await _orderService.CreateAsync(createOrderRequest);
            Log.Information($"Successfully created order with id: {order.Id}");
            return Ok(order.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to create order with warehouse Id {createOrderRequest.WarehouseId}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(OrderDto), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<OrderDto>> GetByIdAsync([FromQuery] string id)
    {
        try
        {
            var order = await _orderService.GetByIdAsync(id);
            Log.Information($"Successfully got order with id {id}");
            return Ok(order.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to get order with id {id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<OrderDto>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }
    
    [HttpGet("get-all")]
    [ProducesResponseType(typeof(OrderDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<OrderDto>> GetAllAsync([FromQuery] OrderPagingFilteringModel orderPagingFilteringModel)
    {
        try
        {
            var orders = await _orderService
                .GetAllAsync(orderPagingFilteringModel.OrderDate,
                    orderPagingFilteringModel.WarehouseId,
                    orderPagingFilteringModel.ProdId,
                    orderPagingFilteringModel.GetPageIndex(),
                    orderPagingFilteringModel.GetPageSize());

            var result = new OrderListModel();
            result.PagingFilteringContext.LoadPagedList(orders);
            result.Orders = orders.Select(order => order.ToDto()).ToList();
            Log.Information("Successfully got all orders");
            return Ok(result);

        }
        catch (Exception e)
        {
            Log.Error($"Failed to get all orders. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<OrderListModel>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }
  
    // PUT api/order/{id}/status
    /*[HttpPut]
    [Route("/{id}/status")]
    public async Task<ActionResult> UpdateStatus1(string id, [FromBody] int orderStatusValue)
    {
        try
        {
            await _orderService.UpdateOrderStatus(id, orderStatusValue);
            return Ok();

        }
        catch (Exception e)
        {
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }
    */




    [HttpPut("updateOrderStatusId")]
    [ProducesResponseType(typeof(UpdateOrderStatusIdRequest), (int) HttpStatusCode.OK)]

    public async Task<ActionResult> UpdateStatus([FromBody]UpdateOrderStatusIdRequest updateOrderStatusIdRequest)
    {
        try
        {
            await _orderService.UpdateOrderStatusIdAsync(updateOrderStatusIdRequest);
            return Ok();

        }
        catch (Exception e)
        {
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }
}






















// PUT api/order/{id}/status
// [HttpPut]
// //[Route("/{id}/status")]
// public async Task<ActionResult> UpdateOrderStatusIdRequest([FromBody] UpdateOrderStatusIdRequest updateOrderStatusIdRequest)
// {
//     try
//     {
//         await _orderService.;
//         return Ok();
//
//     }
//     catch (Exception e)
//     {
//         Log.Error(e.StackTrace);
//         return BadRequest(e.Message);
//     }
//
// }
