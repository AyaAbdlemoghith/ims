using System.Net;
using Microsoft.AspNetCore.Mvc;
using IMS.API.Extensions;
using IMS.API.DTOs.ShipmentDTOs;
using IMS.API.RequestModels.ShipmentReqModel;
using IMS.API.Services.Interfaces;
using Serilog;
using SnoonuCommons.Helpers;
namespace IMS.API.Controllers;
 
[ApiController]
[Route("api/v1/[controller]")]
public class ShipmentController : Controller
{
    private readonly IShipmentService _shipmentService;

    public ShipmentController(IShipmentService shipmentService)
    {
        _shipmentService = shipmentService;
    }

   [HttpPost]
    [ProducesResponseType(typeof(ShipmentDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<ShipmentDto>> CreateAsync([FromBody] CreateShipmentRequest createShipmentRequest)
    {
        try
        {
            var shipment = await _shipmentService.CreateAsync(createShipmentRequest);
            Log.Information($"Successfully created shipment with id: {shipment.Id}");
            return Ok(shipment.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to create shipment with warehouse Id {createShipmentRequest.WarehouseId}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(ShipmentDto), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ShipmentDto>> GetByIdAsync([FromQuery] string id)
    {
        try
        {
            var shipment = await _shipmentService.GetByIdAsync(id);
            Log.Information($"Successfully got shipment with id {id}");
            return Ok(shipment.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to get shipment with id {id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<ShipmentDto>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }
    
    [HttpGet("get-all")]
    [ProducesResponseType(typeof(ShipmentDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<ShipmentDto>> GetAllAsync([FromQuery] ShipmentPagingFilteringModel shipmentPagingFilteringModel)
    {
        try
        {
            var shipments = await _shipmentService
                .GetAllAsync(shipmentPagingFilteringModel.ShipmentDate,
                    shipmentPagingFilteringModel.WarehouseId,
                    shipmentPagingFilteringModel.ProdId,
                    shipmentPagingFilteringModel.GetPageIndex(),
                    shipmentPagingFilteringModel.GetPageSize());

            var result = new ShipmentListModel();
            result.PagingFilteringContext.LoadPagedList(shipments);
            result.Shipments = shipments.Select(shipment => shipment.ToDto()).ToList();
            Log.Information("Successfully got all shipments");
            return Ok(result);

        }
        catch (Exception e)
        {
            Log.Error($"Failed to get all shipments. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<ShipmentListModel>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }
}