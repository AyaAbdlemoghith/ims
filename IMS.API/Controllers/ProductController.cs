using System.Net;
using Microsoft.AspNetCore.Mvc;
using IMS.API.Extensions;
using IMS.API.DTOs.ProductDTOs;
using IMS.API.RequestModels.ProductReqModel;
using IMS.API.Services;
using IMS.API.Services.Interfaces;
using Serilog;
using SnoonuCommons.Helpers;
namespace IMS.API.Controllers;
 
[ApiController]
[Route("api/v1/[controller]")]
public class ProductController : Controller
{
    private readonly IProductService _productService;

    public ProductController(IProductService productService)
    {
        _productService = productService;
    }

   [HttpPost]
    [ProducesResponseType(typeof(ProductDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<ProductDto>> CreateAsync([FromBody] CreateProductRequest createProductRequest)
    {
        try
        {
            var product = await _productService.CreateAsync(createProductRequest);
            Log.Information($"Successfully created product with id: {product.Id}");
            return Ok(product.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to create product with name {createProductRequest.ProductName}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(ProductDto), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ProductDto>> GetByIdAsync([FromQuery] string id)
    {
        try
        {
            var product = await _productService.GetByIdAsync(id);
            Log.Information($"Successfully got product with id {id}");
            return Ok(product.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to get product with id {id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<ProductDto>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }
    [HttpPut]
    [ProducesResponseType(typeof(ProductDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<ProductDto>> UpdateAsync([FromBody] UpdateProductRequest updateProductRequest)
    {
        try
        {
            var product = await _productService.GetByIdAsync(updateProductRequest.Id);
            if (product.Id != updateProductRequest.Id)
            {
                return Forbid();
            }
                
            await _productService.UpdateAsync(product, updateProductRequest);
            Log.Information($"Successfully updated product with id: {product.Id}");
            return Ok(product.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to update product with id {updateProductRequest.Id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }

    
    [HttpDelete]
    [ProducesResponseType(typeof(IActionResult), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> DeleteAsync([FromQuery] string id)
    {
        try
        {
            var product = await _productService.GetByIdAsync(id);
            await _productService.DeleteAsync(product);//Possible null reference argument for parameter 'product' in 'IMS.API.Services.Interfaces.IProductService.DeleteAsync'
            Log.Information($"Successfully deleted the product with id {id}");
            return Ok();
        }
        catch (Exception e)
        {
            Log.Error($"Failed to delete product with id {id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<IActionResult>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }

    [HttpGet("get-all")]
    [ProducesResponseType(typeof(ProductDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<ProductDto>> GetAllAsync([FromQuery] ProductPagingFilteringModel productPagingFilteringModel)
    {
        try
        {
            var products = await _productService
                .GetAllAsync(productPagingFilteringModel.WarehouseId,
                    productPagingFilteringModel.ProductPrice,
                    productPagingFilteringModel.GetCategoryId(),
                    productPagingFilteringModel.GetPageIndex(),
                    productPagingFilteringModel.GetPageSize());

            var result = new ProductListModel();
            result.PagingFilteringContext.LoadPagedList(products);
            result.Products = products.Select(product => product.ToDto()).ToList();
            Log.Information("Successfully got all products");
            return Ok(result);

        }
        catch (Exception e)
        {
            Log.Error($"Failed to get all products. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<ProductListModel>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }
}