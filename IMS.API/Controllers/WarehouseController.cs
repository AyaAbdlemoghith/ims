using System.Net;
using Microsoft.AspNetCore.Mvc;
using IMS.API.Extensions;
using IMS.API.DTOs;
using IMS.API.RequestModels;
using IMS.API.RequestModels.WarehouseReqModel;
using IMS.API.Services.Interfaces;
using Serilog;
using SnoonuCommons.Helpers;
namespace IMS.API.Controllers;
 
[ApiController]
[Route("api/v1/[controller]")]
public class WarehouseController : Controller
{
    private readonly IWarehouseService _warehouseService;

    public WarehouseController(IWarehouseService warehouseService)
    {
        _warehouseService = warehouseService;
    }

    [HttpPost]
    [ProducesResponseType(typeof(WarehouseDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<WarehouseDto>> CreateAsync([FromBody] CreateWarehouseRequest createWarehouseRequest)
    {
        try
        {
            var warehouse = await _warehouseService.CreateAsync(createWarehouseRequest);
            Log.Information($"Successfully created warehouse with id: {warehouse.Id}");
            return Ok(warehouse.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to create warehouse with name {createWarehouseRequest.WarehouseName}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(WarehouseDto), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<WarehouseDto>> GetByIdAsync([FromQuery] string id)
    {
        try
        {
            var warehouse = await _warehouseService.GetByIdAsync(id);
            Log.Information($"Successfully got warehouse with id {id}");
            return Ok(warehouse.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to get warehouse with id {id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<WarehouseDto>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }
    
    [HttpPut]
    [ProducesResponseType(typeof(WarehouseDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<WarehouseDto>> UpdateAsync([FromBody] UpdateWarehouseRequest updateWarehouseRequest)
    {
        try
        {
            var warehouse = await _warehouseService.GetByIdAsync(updateWarehouseRequest.Id);
            if (warehouse.Id != updateWarehouseRequest.Id)
            {
                return Forbid();
            }
                
            await _warehouseService.UpdateAsync(warehouse, updateWarehouseRequest);
            Log.Information($"Successfully updated warehouse with id: {warehouse.Id}");
            return Ok(warehouse.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to update warehouse with id {updateWarehouseRequest.Id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }
    
    [HttpGet("get-all")]
    [ProducesResponseType(typeof(WarehouseDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<WarehouseDto>> GetAllAsync([FromQuery] WarehousePagingFilteringModel warehousePagingFilteringModel)
    {
        try
        {
            var warehouses = await _warehouseService
                .GetAllAsync(warehousePagingFilteringModel.WarehouseName,
                    warehousePagingFilteringModel.WarehouseLocation,
                    warehousePagingFilteringModel.GetPageIndex(),
                    warehousePagingFilteringModel.GetPageSize());

            var result = new WarehouseListModel();
            result.PagingFilteringContext.LoadPagedList(warehouses);
            result.Warehouses = warehouses.Select(warehouse => warehouse.ToDto()).ToList();
            Log.Information("Successfully got all warehouses");
            return Ok(result);

        }
        catch (Exception e)
        {
            Log.Error($"Failed to get all warehouses. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<WarehouseListModel>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }
}