using FalconFlexCommon.Extensions;
using FalconFlexCommon.Mongo;
using FalconFlexCommon.Redis;
using FalconFlexCommon.Swagger;
using FalconFlexCommon.TypeSearchers;
using FalconFlexCommon.UserContext;
using IMS.API.HostedServices;
using IMS.API.Services;
using IMS.API.Services.Interfaces;
using IMS.Domain.UnitOfWork;
using IMS.Domain.UnitOfWork.Interfaces;
using Microsoft.OpenApi.Models;
using Serilog;
using static FalconFlexCommon.AutoMapper.Extensions;
namespace IMS.API;

public class Startup
{
  public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCustomMvc();
            services.AddCustomAuth();
            services.AddControllers();
            services.AddSwaggerDocs();
            services.AddMongo();
            services.AddWorkContext();
            services.AddHttpClient();
            services.AddTypeSearcher();
            services.AddRedisClient();
            // Add services to the container.
            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
            services.AddScoped<IWarehouseService, WarehouseService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IShipmentService, ShipmentService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddSingleton<ExpiredProductScheduler>();
            #region DatabaseInit
            services.AddHostedService<DatabaseInitHostedService>();
            #endregion
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "IMS", Version = "v1" });
            });
            
        }

       // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,IServiceProvider services,ITypeSearcher typeSearcher,ExpiredProductScheduler expiredProductScheduler)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "IMS v1"));
            }

            app.UseHttpsRedirection();

            AddAutoMapper(typeSearcher);
            app.UseSwaggerDocs();
            app.UseServiceId();
            app.UseSerilogRequestLogging();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseWorkContext();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            // Start the expired product scheduler
            expiredProductScheduler.Start();
        }   
}