using FalconFlexCommon.Extensions;
using FalconFlexCommon.Logging;

namespace IMS.API;

public class Program
{
    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); })
            .UseLogging()
            .ConfigureAppConfiguration((context, config) =>
            {
                config.SetBasePath(CommonHelper.GetAspNetCoreBasePath());
                config.AddJsonFile(
                    $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json",
                    false, true);
                config.AddEnvironmentVariables();
            });
    }
}