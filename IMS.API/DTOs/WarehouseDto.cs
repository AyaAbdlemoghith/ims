using System.ComponentModel.DataAnnotations;
using FalconFlexCommon.Mongo.BaseClasses;

namespace IMS.API.DTOs;

public record WarehouseDto:BaseApiEntityModel
{

    [Required]public string WarehouseName { get; init; }
    
    [Required]public string WarehouseLocation { get; init; }
}