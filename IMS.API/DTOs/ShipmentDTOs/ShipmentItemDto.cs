
using System.ComponentModel.DataAnnotations;
using IMS.Domain.Enums;

namespace IMS.API.DTOs.ShipmentDTOs;

public record ShipmentItemDto
{
    [Required] public string ProdId { get; init; }
    
    [Required] public int ShipmentQuantity { get; init; }
    public ShipmentItemDto(string prodId, int shipmentItem)
    {
        ProdId = prodId;
        ShipmentQuantity = shipmentItem; 
    }
}