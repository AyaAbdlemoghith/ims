using System.ComponentModel.DataAnnotations;
using FalconFlexCommon.Mongo.BaseClasses;
 
namespace IMS.API.DTOs.ShipmentDTOs;

public record ShipmentDto: BaseApiEntityModel
{
    [Required] public DateTime ShipmentDate { get; init; }
    [Required] public string WarehouseId { get; init; }
    [Required] public List<ShipmentItemDto> ShipmentItemsDto { get; init; }
}