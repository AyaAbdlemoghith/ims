using System.ComponentModel.DataAnnotations;
using FalconFlexCommon.Mongo.BaseClasses;

using IMS.Domain.Entities.Order;
 
namespace IMS.API.DTOs.OrderDTOs;

public record OrderDto: BaseApiEntityModel
{
    [Required] public DateTime OrderDate { get; init; }
    [Required] public string WarehouseId { get; init; }
    [Required] public List<OrderItemDto> OrderItemsDto { get; init; }
    [Required] public int OrderStatusId { get; init; }


}