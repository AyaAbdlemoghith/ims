using System.ComponentModel.DataAnnotations;

namespace IMS.API.DTOs.OrderDTOs;

public record OrderItemDto
{
    [Required] public string ProdId { get; init; }
    
    [Required] public int OrderQuantity { get; init; }

    public OrderItemDto(string prodId, int orderQuantity)
    {
        ProdId = prodId;
        OrderQuantity = orderQuantity; 
    }//You have to make a constructor for this because it's value object. 

}