using IMS.Domain.Entities.Product;
using System.ComponentModel.DataAnnotations;
using FalconFlexCommon.Mongo.BaseClasses;
using IMS.Domain.Enums;

namespace IMS.API.DTOs.ProductDTOs;

public record ProductDto 
{
    [Required] public string ProductName { get; init; }

    [Required] public decimal ProductPrice { get; init; }

    [Required] public int CategoryId { get; init; }

    [Required] public DateTime? ProductExpiryDate { get; init; }
    
    [Required] public List<ProductItemDto> ProductItemsDto { get; init; }
 
}