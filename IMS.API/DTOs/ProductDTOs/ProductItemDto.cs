using System.ComponentModel.DataAnnotations;

namespace IMS.API.DTOs.ProductDTOs;

public record ProductItemDto
{
    [Required]public string WarehouseId {get; init; }
    
    [Required] public int ProdQuantity { get; init; }

    public ProductItemDto(string warehouseId,int prodQuantity)
    {
        WarehouseId= warehouseId;
        ProdQuantity= prodQuantity;
    }
}