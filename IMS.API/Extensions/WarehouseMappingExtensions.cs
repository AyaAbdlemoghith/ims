using FalconFlexCommon.Extensions;
using IMS.API.DTOs;
using IMS.API.RequestModels;
using IMS.API.RequestModels.WarehouseReqModel;
using IMS.Domain.Entities;

namespace IMS.API.Extensions;

public static class WarehouseMappingExtensions
{
    #region Warehouse
    public static Warehouse ToWarehouse(this CreateWarehouseRequest request)
    {
        
        return request.MapTo<CreateWarehouseRequest, Warehouse>();
    }
     
    public static Warehouse ToExisting(this UpdateWarehouseRequest request, Warehouse warehouse)
    {
        return warehouse.Update(
            request.WarehouseName,
            request.WarehouseLocation
            );
    }
    
    
    public static WarehouseDto ToDto(this Warehouse warehouse)
    {
        return warehouse.MapTo<Warehouse, WarehouseDto>();
    }
    #endregion
    
}
