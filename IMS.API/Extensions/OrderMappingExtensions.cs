using FalconFlexCommon.Extensions;
using IMS.API.DTOs.OrderDTOs;
using IMS.API.RequestModels.OrderReqModel;
using IMS.Domain.Entities.Order;
using IMS.Domain.Enums;

namespace IMS.API.Extensions;

public static class OrderMappingExtensions
{
    #region Order
    public static Order ToOrder(this CreateOrderRequest request)
    {
        return request.MapTo<CreateOrderRequest, Order>();
    }
    
    public static int ToExisting(this UpdateOrderStatusIdRequest request, Order order)
    {
        return order.OrderStatusId = request.OrderStatusId;
    }
    
    public static OrderDto ToDto(this Order order)
    {
        return order.MapTo<Order, OrderDto>();
    }
    #endregion
    #region OrderItem
    
   
    public static OrderItem ToOrderItem(this OrderItemDto dto)
    {
        return new OrderItem(dto.ProdId, dto.OrderQuantity);
    }
    public static OrderItemDto ToDto(this OrderItem orderItem)
    {
        return new OrderItemDto(orderItem.ProdId, orderItem.OrderQuantity);
    }
    #endregion
 
}