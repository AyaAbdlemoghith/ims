using FalconFlexCommon.Extensions;
using IMS.API.DTOs.ShipmentDTOs;
using IMS.API.RequestModels.ShipmentReqModel;
using IMS.Domain.Entities.Shipment;

namespace IMS.API.Extensions;

public static class ShipmentMappingExtensions
{
    #region Shipment
    public static Shipment ToShipment(this CreateShipmentRequest request)
    {
        return request.MapTo<CreateShipmentRequest, Shipment>();
    }
    
    
    public static ShipmentDto ToDto(this Shipment shipment)
    {
        return shipment.MapTo<Shipment, ShipmentDto>();
    }
    #endregion
    #region ShipmentItem
    
   
    public static ShipmentItem ToShipmentItem(this ShipmentItemDto dto)
    {
        return new ShipmentItem(dto.ProdId, dto.ShipmentQuantity);
    }
    public static ShipmentItemDto ToDto(this ShipmentItem shipmentItem)
    {
        return new ShipmentItemDto(shipmentItem.ProdId, shipmentItem.ShipmentQuantity);
    }
    #endregion
}