using FalconFlexCommon.Extensions;
using IMS.API.DTOs.ProductDTOs;
using IMS.API.RequestModels.ProductReqModel;
using IMS.Domain.Entities.Product;

namespace IMS.API.Extensions;

public static class ProductMappingExtensions
{
    #region Product

    public static Product ToProduct(this CreateProductRequest request)
    {
        return request.MapTo<CreateProductRequest, Product>();
    }
    public static Product ToExisting(this UpdateProductRequest request, Product product)
    {
        return product.Update(
           request.ProductName,
           request.ProductPrice,
           request.CategoryId,
           request.ProductExpiryDate,
           request.ProductItemsDto?.Select(pq => pq.ToProductItem()).ToList());
   }
    public static ProductDto ToDto(this Product product)
    {
        return product.MapTo<Product, ProductDto>();
    }
    #endregion
    
    #region ProductItem
    
   
    public static ProductItem ToProductItem(this ProductItemDto dto)
    {
        return new ProductItem(dto.WarehouseId, dto.ProdQuantity);
    }
    public static ProductItemDto ToDto(this ProductItem productItem)
    {
        return new ProductItemDto(productItem.WarehouseId, productItem.ProdQuantity);
    }
    #endregion
}
