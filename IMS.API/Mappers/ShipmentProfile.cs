using AutoMapper;
using FalconFlexCommon.AutoMapper;
using IMS.API.DTOs.ShipmentDTOs;
using IMS.API.Extensions;
using IMS.API.RequestModels.ShipmentReqModel;
using IMS.Domain.Entities.Shipment;

namespace IMS.API.Mappers;

public class ShipmentProfile : Profile, IAutoMapperProfile
{
    public ShipmentProfile()
    {
        CreateMap<CreateShipmentRequest, Shipment>()
            .ConstructUsing(src => new Shipment(
                src.ShipmentDate,
                src.WarehouseId,
                src.ShipmentItemsDto.Select(pq => pq.ToShipmentItem()).ToList()
            )).ForAllMembers(opt => opt.Ignore());
        CreateMap<Shipment, ShipmentDto>()
            .ForMember(dest => dest.ShipmentItemsDto, opt => opt.MapFrom(src => src.ShipmentItems.Select(s => s.ToDto()).ToList()));
    }
}