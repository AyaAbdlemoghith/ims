using AutoMapper;
using FalconFlexCommon.AutoMapper;
using IMS.API.DTOs.ProductDTOs;
using IMS.API.Extensions;
using IMS.API.RequestModels.ProductReqModel;
using IMS.Domain.Entities.Product;

namespace IMS.API.Mappers;

public class ProductProfile : Profile, IAutoMapperProfile
{
    public ProductProfile()
    {
        CreateMap<CreateProductRequest, Product>()
            .ConstructUsing(src => new Product(
                    src.ProductName,
                    src.ProductPrice,
                    src.CategoryId,
                    src.ProductExpiryDate,
                    src.ProductItemsDto.Select(pq => pq.ToProductItem()).ToList())
            ).ForAllMembers(opt => opt.Ignore());

        CreateMap<Product, ProductDto>()
            .ForMember(dest => dest.ProductItemsDto, opt => opt.MapFrom(src => src.ProductItems.Select(s => s.ToDto()).ToList()));
    }
}