using AutoMapper;
using FalconFlexCommon.AutoMapper;
using IMS.API.DTOs;
using IMS.API.RequestModels.WarehouseReqModel;
using IMS.Domain.Entities;

namespace IMS.API.Mappers;

public class WarehouseProfile : Profile, IAutoMapperProfile
{
    public WarehouseProfile()
    {
        CreateMap<CreateWarehouseRequest, Warehouse>()
            .ConstructUsing(src => new Warehouse(
                    src.WarehouseName,
                    src.WarehouseLocation
                    )).ForAllMembers(opt => opt.Ignore());
        CreateMap<Warehouse, WarehouseDto>();
    }
}
