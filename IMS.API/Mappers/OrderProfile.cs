using AutoMapper;
using FalconFlexCommon.AutoMapper;
using IMS.API.DTOs.OrderDTOs;
using IMS.API.Extensions;
using IMS.API.RequestModels.OrderReqModel;
using IMS.Domain.Entities.Order;

namespace IMS.API.Mappers;

public class OrderProfile : Profile, IAutoMapperProfile
{
    public OrderProfile()
    {
        CreateMap<CreateOrderRequest, Order>()
            .ConstructUsing(src => new Order(
                src.OrderDate,
                src.WarehouseId,
                src.OrderItemsDto.Select(pq => pq.ToOrderItem()).ToList(),
                src.OrderStatusId
            )).ForAllMembers(opt => opt.Ignore());
        CreateMap<Order, OrderDto>()
            .ForMember(dest => dest.OrderItemsDto, opt => opt.MapFrom(src => src.OrderItems.Select(s => s.ToDto()).ToList()));
    }
}