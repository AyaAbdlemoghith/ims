using FalconFlexCommon.Mongo.Pagination;
using IMS.Domain.Enums;

namespace IMS.API.RequestModels.ProductReqModel;

public class ProductPagingFilteringModel : BasePageableModel
{
    public string? WarehouseId { get; set; }
    public decimal? ProductPrice { get; set; }
    public int? CategoryId { get; set; }
    public virtual Category?  GetCategoryId()
    {
        return CategoryId != null ? Category.FromValue(CategoryId.Value) : null;
    }

    public virtual int GetPageNumber()
    {
        if (PageNumber <= 0) PageNumber = 1;
        return PageNumber;
    }

    public virtual int GetPageSize()
    {
        if (PageSize is <= 0 or >= 1000) PageSize = 100;
        return PageSize;
    }

    public virtual int GetPageIndex()
    {
        return GetPageNumber() - 1;
    }
}