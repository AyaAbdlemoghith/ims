using System.ComponentModel.DataAnnotations;
using IMS.API.DTOs.ProductDTOs;


namespace IMS.API.RequestModels.ProductReqModel;

public record CreateProductRequest
{
    [Required] public string ProductName { get; set; }

    [Required] public decimal ProductPrice { get; init; }   
    
    [Required]  public int CategoryId { get; init; }
    
    public DateTime? ProductExpiryDate { get; init; }
    [Required] public List<ProductItemDto> ProductItemsDto { get; set; }

}


