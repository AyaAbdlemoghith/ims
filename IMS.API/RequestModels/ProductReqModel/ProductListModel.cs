using IMS.API.DTOs.ProductDTOs;

namespace IMS.API.RequestModels.ProductReqModel;

public class ProductListModel
{
    public ProductListModel()
    {
        PagingFilteringContext = new ProductPagingFilteringModel();
        Products = new List<ProductDto>();
    }

    public ProductPagingFilteringModel PagingFilteringContext { get; set; }
    public IList<ProductDto> Products { get; set; }
}