using System.ComponentModel.DataAnnotations;
using IMS.API.DTOs.ProductDTOs;
using IMS.Domain.Entities.Product;
using IMS.Domain.Enums;

namespace IMS.API.RequestModels.ProductReqModel;

public class UpdateProductRequest
{
     [Required] public string Id { get; set; }//The controller will use it to update specific prod by its Id
     public string? ProductName { get; set; }
     
     public decimal? ProductPrice { get; init; }   
    
     public int? CategoryId { get; set; }

     public DateTime? ProductExpiryDate { get; init; }
     public List<ProductItemDto>? ProductItemsDto { get; set; }
     
 
      
}