using System.ComponentModel.DataAnnotations;
using IMS.API.DTOs.OrderDTOs;
using MongoDB.Entities;

namespace IMS.API.RequestModels.OrderReqModel;

public record CreateOrderRequest
{
 
    [Required]public DateTime OrderDate { get; set; }

    [Required]public string WarehouseId { get; set; }
    
    [Required]public List<OrderItemDto> OrderItemsDto { get; set; } 

    public int OrderStatusId => 1;
}


