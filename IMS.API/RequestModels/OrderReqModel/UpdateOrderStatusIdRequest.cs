using System.ComponentModel.DataAnnotations;

namespace IMS.API.RequestModels.OrderReqModel;

public class UpdateOrderStatusIdRequest
{
    [Required] public string id { get; set; }
    [Required]public int OrderStatusId { get; set; }

}