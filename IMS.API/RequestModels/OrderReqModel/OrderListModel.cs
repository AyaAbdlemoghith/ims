using IMS.API.DTOs.OrderDTOs;

namespace IMS.API.RequestModels.OrderReqModel;

public class OrderListModel
{
    public OrderListModel()
    {
        PagingFilteringContext = new OrderPagingFilteringModel();
        Orders = new List<OrderDto>();
    }

    public OrderPagingFilteringModel PagingFilteringContext { get; set; }
    public IList<OrderDto> Orders { get; set; }
}