using IMS.API.DTOs.ShipmentDTOs;

namespace IMS.API.RequestModels.ShipmentReqModel;

public class ShipmentListModel
{
    public ShipmentListModel()
    {
        PagingFilteringContext = new ShipmentPagingFilteringModel();
        Shipments = new List<ShipmentDto>();
    }

    public ShipmentPagingFilteringModel PagingFilteringContext { get; set; }
    public IList<ShipmentDto> Shipments { get; set; }
}