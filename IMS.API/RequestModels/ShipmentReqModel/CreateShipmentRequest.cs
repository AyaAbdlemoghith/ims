using System.ComponentModel.DataAnnotations;
using IMS.API.DTOs.ShipmentDTOs;
 
namespace IMS.API.RequestModels.ShipmentReqModel;

public record CreateShipmentRequest
{
 
    [Required]public DateTime ShipmentDate { get; set; }
    [Required]public string WarehouseId { get; set; }
    
    [Required]public List<ShipmentItemDto> ShipmentItemsDto { get; set; } 
   
 }


