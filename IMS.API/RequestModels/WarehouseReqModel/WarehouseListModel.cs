using IMS.API.DTOs;

namespace IMS.API.RequestModels;

public class WarehouseListModel
{
    public WarehouseListModel()
    {
        PagingFilteringContext = new WarehousePagingFilteringModel();
        Warehouses = new List<WarehouseDto>();
    }

    public WarehousePagingFilteringModel PagingFilteringContext { get; set; }
    public IList<WarehouseDto> Warehouses { get; set; }
}