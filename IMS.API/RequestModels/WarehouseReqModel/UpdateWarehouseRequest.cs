using System.ComponentModel.DataAnnotations;
using IMS.API.DTOs;
using IMS.Domain.Entities;

namespace IMS.API.RequestModels;

public class UpdateWarehouseRequest
{
     [Required] public string Id { get; set; }
     public string WarehouseName { get; set; }
     public string WarehouseLocation { get; set; }
 
     public UpdateWarehouseRequest (string? warehouseName, string? warehouseLocation )
     {
          WarehouseName = warehouseName;
          WarehouseLocation = warehouseLocation;
     }
}