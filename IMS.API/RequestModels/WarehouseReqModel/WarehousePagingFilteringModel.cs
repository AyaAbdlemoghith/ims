using FalconFlexCommon.Mongo.Pagination;

namespace IMS.API.RequestModels;

public class WarehousePagingFilteringModel : BasePageableModel
{
    public string? WarehouseName { get; set; }
    public string? WarehouseLocation { get; set; }
  
    public virtual int GetPageNumber()
    {
        if (PageNumber <= 0) PageNumber = 1;
        return PageNumber;
    }

    public virtual int GetPageSize()
    {
        if (PageSize is <= 0 or >= 1000) PageSize = 100;
        return PageSize;
    }

    public virtual int GetPageIndex()
    {
        return GetPageNumber() - 1;
    }
}