using System.ComponentModel.DataAnnotations;

namespace IMS.API.RequestModels.WarehouseReqModel;

public record CreateWarehouseRequest
{
 
   [Required] public string WarehouseName { get; init; }
   [Required] public string WarehouseLocation { get; init; }
}


