using IMS.API.Services.Interfaces;

namespace IMS.API.Services
{
    public class ExpiredProductScheduler
    {
        private readonly IServiceProvider _serviceProvider;
        private Timer _timer;

        public ExpiredProductScheduler(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            TimeSpan interval = TimeSpan.FromDays(14);

            // Initialize the timer but don't start it yet
            _timer = new Timer(RunCheckExpiredProducts, null, TimeSpan.Zero, interval);
        }

        public void Start()
        {
            // Start the timer
            _timer.Change(TimeSpan.Zero, TimeSpan.FromDays(14));
        }

        private async void RunCheckExpiredProducts(object? state)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var productService = scope.ServiceProvider.GetRequiredService<IProductService>();
                await productService.CheckExpiredProducts();
            }
        }
    }
}
