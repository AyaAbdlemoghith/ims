using FalconFlexCommon.Mongo.Pagination;
using IMS.API.RequestModels;
using IMS.API.RequestModels.WarehouseReqModel;
using IMS.Domain.Entities;


namespace IMS.API.Services.Interfaces;

public interface IWarehouseService
{
    public Task<Warehouse> CreateAsync(CreateWarehouseRequest createWarehouseRequest);
    public Task<Warehouse> UpdateAsync(Warehouse warehouse, UpdateWarehouseRequest updateWarehouseRequest);
    public Task<Warehouse> GetByIdAsync(string id);
    public Task<IPagedList<Warehouse>> GetAllAsync(
        string? warehouseName = null, string? warehouseLocation = null,
        int pageIndex = 0, int pageSize = Int32.MaxValue);
}