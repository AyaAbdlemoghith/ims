using FalconFlexCommon.Mongo.Pagination;

using IMS.API.RequestModels.ProductReqModel;
using IMS.Domain.Entities.Product;
using IMS.Domain.Enums;


namespace IMS.API.Services.Interfaces;

public interface IProductService
{
    public Task<Product> CreateAsync(CreateProductRequest createProductRequest);
    public Task<Product> CreateAsync(Product product);

    public Task<Product> UpdateAsync(Product product, UpdateProductRequest updateProductRequest);
    public Task DeleteAsync(Product product);
    public Task<Product?> GetByIdAsync(string id);
    public Task<IPagedList<Product>> GetAllAsync(
        string? warehouseId = null, decimal? priceProd= null, Category? category= null,
        int pageIndex = 0, int pageSize = Int32.MaxValue);
    public Task CheckExpiredProducts();

    public IQueryable<Product> Table();

}