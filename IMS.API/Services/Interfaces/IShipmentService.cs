using FalconFlexCommon.Mongo.Pagination;
using IMS.API.RequestModels.ShipmentReqModel;
using IMS.Domain.Entities.Shipment;


namespace IMS.API.Services.Interfaces;

public interface IShipmentService
{
    public Task<Shipment> CreateAsync(CreateShipmentRequest createShipmentRequest);
    
    public Task<Shipment> GetByIdAsync(string id);
    
    public Task<IPagedList<Shipment>> GetAllAsync(
        DateTime? shipmentDate = null, string? warehouseId = null,
        string? prodId = null,
        int pageIndex = 0, int pageSize = Int32.MaxValue);
}