using FalconFlexCommon.Mongo.Pagination;
using IMS.API.RequestModels.OrderReqModel;
using IMS.Domain.Entities.Order;
 

namespace IMS.API.Services.Interfaces;

public interface IOrderService
{
    public Task<Order> CreateAsync(CreateOrderRequest createOrderRequest);
    
    public Task<Order> GetByIdAsync(string id);
    
    public Task<IPagedList<Order>> GetAllAsync(
        DateTime? orderDate = null, string? warehouseId = null,
        string? prodId = null,
        int pageIndex = 0, int pageSize = Int32.MaxValue);
   public Task UpdateOrderStatusIdAsync(UpdateOrderStatusIdRequest updateOrderStatusIdRequest);
    
}