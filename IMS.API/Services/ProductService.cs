using FalconFlexCommon.Mongo.Pagination;
using IMS.API.Extensions;
using IMS.API.RequestModels.ProductReqModel;
using IMS.API.Services.Interfaces;
using IMS.Domain.Entities.Product;
using IMS.Domain.Enums;
using IMS.Domain.UnitOfWork.Interfaces;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace IMS.API.Services;

public class ProductService : IProductService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IWarehouseService _warehouseService;

  
    public ProductService(IUnitOfWork unitOfWork, IWarehouseService warehouseService)
    {
        _unitOfWork = unitOfWork;
        _warehouseService = warehouseService;

    }

    public async Task<Product> CreateAsync(CreateProductRequest createProductRequest)
    {
        var warehouseIds=createProductRequest.ProductItemsDto.Select(dto => dto.WarehouseId).ToList();
        foreach(var warehouseId in warehouseIds)
        {
            var warehouse = await _warehouseService.GetByIdAsync(warehouseId);
            if (warehouse == null) throw new Exception("Warehouse does not exist. Product creation aborted.");
        }
        var product = createProductRequest.ToProduct();
        await _unitOfWork.ProductRepository.InsertAsync(product);
      
        return product;
    }
    public async Task<Product> CreateAsync(Product product)
    {
        await _unitOfWork.ProductRepository.InsertAsync(product);
        return product;
    }
    
    public async Task<Product> UpdateAsync(Product product, UpdateProductRequest updateProductRequest)
    {
        var updatedProduct = updateProductRequest.ToExisting(product);
        await _unitOfWork.ProductRepository.UpdateAsync(updatedProduct);
     return updatedProduct;
    }
 

    public async Task DeleteAsync(Product product)
    {
         await _unitOfWork.ProductRepository.DeleteAsync(product);
    }


    public async Task<Product?> GetByIdAsync(string id)
    {
        return await _unitOfWork.ProductRepository.GetByIdAsync(id);
    }

    public async Task<List<Product>> GetAllProdsByWarehouseIdAsync(string warehouseId)
    {
        var prodsQuery = _unitOfWork.ProductRepository.Table;
        var productsWithWarehouseId = await prodsQuery
            .Where(p => p.ProductItems.Any(pq => pq.WarehouseId == warehouseId))
            .ToListAsync();
        return productsWithWarehouseId;
    }

    public IQueryable<Product> Table()
    {
        return _unitOfWork.ProductRepository.Table;
    }
    public async Task CheckExpiredProducts()
    {
        var products = Table().ToList();
        DateTime today = DateTime.Now.Date;

        foreach (var product in products)
        {
            if (!product.ProductExpiryDate.HasValue || product.ProductExpiryDate.Value.Date > today) continue;
            await HandleExpiredProduct(product);
        }

    }
    private async Task HandleExpiredProduct(Product product)
    {
        Console.WriteLine($"Product '{product.ProductName}' has expired on {product.ProductExpiryDate}.\n the Product will be delete it");
        await DeleteAsync(product);
        Log.Warning($"Product '{product.ProductName}' has expired on {product.ProductExpiryDate}.\n the Product will be delete it");
    }

    public async Task<IPagedList<Product>> GetAllAsync(
        string? warehouseId = null, decimal? priceProd= null, Category? category= null,
         int pageIndex = 0, int pageSize = Int32.MaxValue)
    {
        var query = _unitOfWork.ProductRepository.Table;
        var query1 = Table();
        
        if (warehouseId != null)
        {
            query = query.Where(product => product.ProductItems.Any(pq => pq.WarehouseId == warehouseId));
        }
        if ( priceProd!= null)
        {            
            query = query.Where(product => product.ProductPrice == priceProd);
        }
        if ( category!= null)
        {     
            query = query.Where(product => product.CategoryId == category.Value);
        }
        return await PagedList<Product>.Create(query, pageIndex, pageSize);
    }
}

