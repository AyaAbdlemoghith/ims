using FalconFlexCommon.Mongo.Pagination;
using IMS.API.Extensions;
using IMS.API.RequestModels.ShipmentReqModel;
using IMS.API.Services.Interfaces;
 using IMS.Domain.Entities.Product;
using IMS.Domain.Entities.Shipment;
using IMS.Domain.UnitOfWork.Interfaces;
 
namespace IMS.API.Services;

public class ShipmentService : IShipmentService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IProductService _productService;
    private readonly IWarehouseService _warehouseService;



    public ShipmentService(IUnitOfWork unitOfWork, IProductService productService, IWarehouseService warehouseService)
    {
        _unitOfWork = unitOfWork;
        _productService = productService;
        _warehouseService = warehouseService;
    }
    
    public async Task<Shipment> CreateAsync(CreateShipmentRequest createShipmentRequest)
    {
        var warehouse = await _warehouseService.GetByIdAsync(createShipmentRequest.WarehouseId);
        if (warehouse == null) throw new Exception("Warehouse does not exist. Shipment creation aborted.");

        //
        await ExistingProduct(createShipmentRequest);
        var shipment = createShipmentRequest.ToShipment();
        AddShipmentInProducts(shipment);
        await _unitOfWork.ShipmentRepository.InsertAsync(shipment);

        return shipment;
    }
    
    private async Task ExistingProduct(CreateShipmentRequest createShipmentRequest)
    {
        var tasks = createShipmentRequest.ShipmentItemsDto.Select(item => _productService.GetByIdAsync(item.ProdId));
        var products = await Task.WhenAll(tasks);
        var areAnyProdIdsNotExist = products.Any(product => product == null);
        if (areAnyProdIdsNotExist) 
            throw new Exception($"Trying to add one or more products with wrong or non-existing Product");
    }
    private async void AddShipmentInProducts(Shipment shipment)
    {
        foreach (var shipmentItem in shipment.ShipmentItems)
        {
             var product = await _productService.GetByIdAsync(shipmentItem.ProdId);
             if (product == null) 
                 throw new Exception($"Product with ProdId {shipmentItem.ProdId} does not exist in Product Collection"); 
             await UpdateMatchedProduct(product, shipmentItem, shipment.WarehouseId);
        }    
    }

    public async Task<Shipment> GetByIdAsync(string id)
    {
        return await _unitOfWork.ShipmentRepository.GetByIdAsync(id);
    }

    public async Task<IPagedList<Shipment>> GetAllAsync(
        DateTime? shipmentDate =null,
        string? warehouseId = null, string? prodId = null,
         int pageIndex = 0, int pageSize = Int32.MaxValue)
    {
        var query = _unitOfWork.ShipmentRepository.Table;
        if (warehouseId != null)
        {
            query = query.Where(shipment => shipment.WarehouseId == warehouseId);
        }
        if ( prodId!= null)
        {     
            query = query.Where(shipment => shipment.ShipmentItems.Any(pq => pq.ProdId == prodId));

        }
        if ( shipmentDate!= null)
        {            
            query = query.Where(shipment => shipment.ShipmentDate == shipmentDate);
        }

        return await PagedList<Shipment>.Create(query, pageIndex, pageSize);
    }

    // Method to update matched product quantities
    public async Task UpdateMatchedProduct(Product matchedProduct, ShipmentItem shipmentItem, string warehouseId)
    {
        var existingProdItem = matchedProduct.ProductItems.FirstOrDefault(pq => pq.WarehouseId == warehouseId);
        
        if (existingProdItem != null)
            existingProdItem.ProdQuantity += shipmentItem.ShipmentQuantity;
        else
            // Product exists but not for this warehouse, add new ProdItem
            matchedProduct.ProductItems.Add(new ProductItem(warehouseId, shipmentItem.ShipmentQuantity));
        await _unitOfWork.ProductRepository.UpdateAsync(matchedProduct);
    }
 

}