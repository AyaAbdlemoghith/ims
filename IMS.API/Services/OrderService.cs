using FalconFlexCommon.Mongo.Pagination;
using IMS.API.Extensions;
using IMS.API.RequestModels.OrderReqModel;
using IMS.API.Services.Interfaces;
using IMS.Domain.Entities.Order;
using IMS.Domain.Entities.Product;
using IMS.Domain.Enums;
using IMS.Domain.UnitOfWork.Interfaces;
 
namespace IMS.API.Services;
public class OrderService : IOrderService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IProductService _productService;
    private readonly IWarehouseService _warehouseService;
 

    public OrderService(IUnitOfWork unitOfWork, IProductService productService, IWarehouseService warehouseService)
    {
        _unitOfWork = unitOfWork;
        _productService = productService;
        _warehouseService = warehouseService;
    }
    
    public async Task<Order> CreateAsync(CreateOrderRequest createOrderRequest)
    {
        var warehouse = await _warehouseService.GetByIdAsync(createOrderRequest.WarehouseId);
        if (warehouse == null) throw new Exception("Warehouse does not exist. Order creation aborted.");
        
        await ExistingProduct(createOrderRequest);
        var order = createOrderRequest.ToOrder();
        await DecreaseStockFromProduct(order);
        await _unitOfWork.OrderRepository.InsertAsync(order);
        return order;
    }

    private async Task DecreaseStockFromProduct(Order order)
    {
        await AllWarehouseIdsMatched(order);
        foreach (var orderItem in order.OrderItems)
        {
            var product = await _unitOfWork.ProductRepository.GetByIdAsync(orderItem.ProdId);
            var existingProductItem = product.ProductItems.FirstOrDefault(pq => pq.WarehouseId == order.WarehouseId);
            UpdateMatchedProduct(orderItem, order.WarehouseId, existingProductItem,product);
        }
    }

    private async Task AllWarehouseIdsMatched(Order order)
    {
        var isAllWarehouseIdsMatched = true;

        foreach (var orderItem in order.OrderItems)
        {
            var product = await _productService.GetByIdAsync(orderItem.ProdId);
             if (product == null) 
                 throw new Exception($"Product with ProdId {orderItem.ProdId} does not exist in Product Collection");             
            var warehouseIdMatched = product.ProductItems.FirstOrDefault(pq => pq.WarehouseId == order.WarehouseId);
             if (warehouseIdMatched != null) continue;
            isAllWarehouseIdsMatched = false;
            break;  
        }
        if (!isAllWarehouseIdsMatched)
            throw new Exception($"One or more product quantities for WarehouseId {order.WarehouseId} do not exist");
    }
    private async Task ExistingProduct(CreateOrderRequest createOrderRequest)
    {
        var tasks = createOrderRequest.OrderItemsDto.Select(item => _productService.GetByIdAsync(item.ProdId));
        var products = await Task.WhenAll(tasks);
        var areAnyProdIdsNotExist = products.Any(product => product == null);
        if (areAnyProdIdsNotExist) 
            throw new Exception($"Trying to add one or more products with wrong or non-existing Product");
    }

    void UpdateMatchedProduct(OrderItem orderItem, string warehouseId,  ProductItem? existingProductItem,Product product)
    {
        if (existingProductItem == null)
            throw new InvalidOperationException("Existing product quantity is null.");

        var subProdOrderFromProd = existingProductItem.ProdQuantity - (orderItem.OrderQuantity);
         if (subProdOrderFromProd < 0)
            throw new Exception($"No enough stock for this WarehouseId {warehouseId}");
         existingProductItem.ProdQuantity -= orderItem.OrderQuantity;
         _unitOfWork.ProductRepository.UpdateAsync(product);
    }
    public async Task<Order> GetByIdAsync(string id)
    {
        return await _unitOfWork.OrderRepository.GetByIdAsync(id);
    }

    public async Task<IPagedList<Order>> GetAllAsync(
        DateTime? orderDate =null,
        string? warehouseId = null, string? prodId = null,
         int pageIndex = 0, int pageSize = Int32.MaxValue)
    {
        var query = _unitOfWork.OrderRepository.Table;
        if (warehouseId != null)
        {
            query = query.Where(order => order.WarehouseId == warehouseId);
        }
        if ( prodId!= null)
        {     
            query = query.Where(order => order.OrderItems.Any(pq => pq.ProdId == prodId));

        }
        if ( orderDate!= null)
        {            
            query = query.Where(order => order.OrderDate == orderDate);
        }

        return await PagedList<Order>.Create(query, pageIndex, pageSize);
    }


    public async Task UpdateOrderStatusIdAsync(UpdateOrderStatusIdRequest updateOrderStatusIdRequest)
    {
        var nextOrderStatus= OrderStatus.FromValue(updateOrderStatusIdRequest.OrderStatusId);
        var order = await GetByIdAsync(updateOrderStatusIdRequest.id);
        var currentStatus = order.OrderStatus;
        if (!currentStatus.IsTransitionToNextStatus(nextOrderStatus.Value))
            throw new Exception($"Invalid status transition.");
        
        order.OrderStatus = nextOrderStatus;
        await _unitOfWork.OrderRepository.UpdateAsync(order);
        if(nextOrderStatus.IsIncreasingStock()) await IncreaseStockFromProduct(order);
    }
    private async Task IncreaseStockFromProduct(Order order)
    {
        foreach (var orderItem in order.OrderItems)
        {
            var product = await _productService.GetByIdAsync(orderItem.ProdId);
            if (product == null) 
                throw new Exception($"Product with ProdId {orderItem.ProdId} does not exist in order quantities"); 
            var existingProdItem = product.ProductItems.FirstOrDefault(pq => pq.WarehouseId == order.WarehouseId);
            if (existingProdItem != null)
                existingProdItem.ProdQuantity += orderItem.OrderQuantity;
            await _unitOfWork.ProductRepository.UpdateAsync(product);        
        }  
    }
 
}