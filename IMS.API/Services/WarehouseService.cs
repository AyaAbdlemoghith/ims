using FalconFlexCommon.Mongo.Pagination;
using IMS.API.Extensions;
using IMS.API.RequestModels;
using IMS.API.RequestModels.WarehouseReqModel;
using IMS.API.Services.Interfaces;
using IMS.Domain.Entities;
using IMS.Domain.UnitOfWork.Interfaces;
 
namespace IMS.API.Services;

public class WarehouseService : IWarehouseService
{
    private readonly IUnitOfWork _unitOfWork;


    public WarehouseService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    public async Task<Warehouse> CreateAsync(CreateWarehouseRequest createWarehouseRequest)
    {
        var warehouse = createWarehouseRequest.ToWarehouse();
        await _unitOfWork.WarehouseRepository.InsertAsync(warehouse);
      
        var text = $"Successfully Created new Warehouse: {warehouse.WarehouseName}";
        return warehouse;
    }
    
    public async Task<Warehouse> UpdateAsync(Warehouse warehouse, UpdateWarehouseRequest updateWarehouseRequest)
    {
      
        var updatedWarehouse = updateWarehouseRequest.ToExisting(warehouse);
        await _unitOfWork.WarehouseRepository.UpdateAsync(updatedWarehouse);
     return updatedWarehouse;
    }
    public async Task<Warehouse> GetByIdAsync(string id)
    {
        return await _unitOfWork.WarehouseRepository.GetByIdAsync(id);
    }

    public async Task<IPagedList<Warehouse>> GetAllAsync(
         string? warehouseLocation= null, string? warehouseName= null,
         int pageIndex = 0, int pageSize = Int32.MaxValue)
    {
        var query = _unitOfWork.WarehouseRepository.Table;
        if ( warehouseLocation!= null)
        {            
            query = query.Where(warehouse => warehouse.WarehouseLocation == warehouseLocation);
        }
        if ( warehouseName!= null)
        {            
            query = query.Where(warehouse => warehouse.WarehouseName == warehouseName);
        }
 
        return await PagedList<Warehouse>.Create(query, pageIndex, pageSize);
    }



}