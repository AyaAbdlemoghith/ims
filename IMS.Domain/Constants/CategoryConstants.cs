namespace IMS.Domain.Constants;

public static class CategoryConstants
{
    public static readonly string Grains= "Grains";
    public const int GrainsValue = 1;

    public static readonly string Dairy= "Dairy";
    public const int DairyValue = 2;
    
    public static readonly string Beverages= "Beverages";
    public const int BeveragesValue = 3;
    
    public static readonly string Snacks= "Snacks";
    public const int SnacksValue = 4;
    
    public static readonly string Fruits= "Fruits";
    public const int FruitsValue = 5;
    
    public static readonly string Pharmaceuticals= "Pharmaceuticals";
    public const int PharmaceuticalsValue = 6;
    
    //No Expiry Needed
    public static readonly string Clothing= "Clothing";
    public const int ClothingValue = 7;
    public static readonly string Electronics= "Electronics";
    public const int ElectronicsValue = 8;
    public static readonly string Furniture= "Furniture";
    public const int FurnitureValue = 9;
    public static readonly string Chemicals= "Chemicals";
    public const int ChemicalsValue = 10;
}
