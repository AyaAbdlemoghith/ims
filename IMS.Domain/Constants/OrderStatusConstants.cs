namespace IMS.Domain.Constants;

public static class OrderStatusConstants
{
    public static readonly string New = "New";
    public const int NewValue = 1;

    public static readonly string IsDelivering = "IsDelivering";
    public const int IsDeliveringValue = 2;
    
    public static readonly string Delivered = "Delivered";
    public const int DeliveredValue = 3;

    public static readonly string Cancelled = "Cancelled";
    public const int CancelledValue = 4;
    

    
        
    
}