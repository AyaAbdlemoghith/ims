using FalconFlexCommon.Mongo.Repositories;
using FalconFlexCommon.UserContext;
using IMS.Domain.Entities;
using IMS.Domain.Repositories.Interfaces;
using MongoDB.Driver;

namespace IMS.Domain.Repositories;

public class WarehouseRepository : IWarehouseRepository
{
    private readonly IRepository<Warehouse> _repository;
    private readonly IWorkContext _workContext;

    public WarehouseRepository(IWorkContext workContext, IRepository<Warehouse> repository)
    {    
        _workContext = workContext;
        _repository = repository;
    }
 
    public async Task<Warehouse> GetByIdAsync(string id)
    {
        var result = await _repository.GetByIdAsync(id);
        //if (result == null) throw Exceptions.CreateNotFoundException(nameof(Warehouse));
        return result;
    }

    public async Task<Warehouse> InsertAsync(Warehouse warehouse, IClientSessionHandle? sessionHandle = null)
    {
        warehouse.InitializeInsert(_workContext.WorkContext.UserId, DateTime.UtcNow);
        return await _repository.InsertAsync(warehouse, sessionHandle);
    }

    public async Task<Warehouse> UpdateAsync(Warehouse warehouse, IClientSessionHandle? sessionHandle = null)
    {
        warehouse.InitializeUpdate(_workContext.WorkContext.UserId, DateTime.UtcNow);
        return await _repository.UpdateAsync(warehouse, sessionHandle);
    }
    public async Task<Warehouse> DeleteAsync(Warehouse warehouse, IClientSessionHandle? sessionHandle = null)
    {
        return await _repository.DeleteAsync(warehouse, sessionHandle);
    }

    public IQueryable<Warehouse> Table => _repository.Table;
}