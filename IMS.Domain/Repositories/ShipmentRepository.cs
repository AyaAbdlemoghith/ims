using FalconFlexCommon.Mongo.Repositories;
using FalconFlexCommon.UserContext;
using IMS.Domain.Entities.Shipment;
using IMS.Domain.Repositories.Interfaces;
using MongoDB.Driver;

namespace IMS.Domain.Repositories;
public class ShipmentRepository : IShipmentRepository
{
    private readonly IRepository<Shipment> _repository;
    private readonly IWorkContext _workContext;
 
    public ShipmentRepository(IWorkContext workContext, IRepository<Shipment> repository)
    {    
        _workContext = workContext;
        _repository = repository;
    }

    public async Task<Shipment> GetByIdAsync(string id)
    {
        var result = await _repository.GetByIdAsync(id);
        if (result == null) throw Exceptions.CreateNotFoundException(nameof(Shipment));;
        return result;
    }

    public async Task<Shipment> InsertAsync(Shipment shipment, IClientSessionHandle? sessionHandle = null)
    {
        shipment.InitializeInsert(_workContext.WorkContext.UserId, DateTime.UtcNow);
        return await _repository.InsertAsync(shipment, sessionHandle);
    }
    public IQueryable<Shipment> Table => _repository.Table;
}