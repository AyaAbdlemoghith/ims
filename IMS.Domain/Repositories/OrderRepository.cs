using FalconFlexCommon.Mongo.Repositories;
using FalconFlexCommon.UserContext;
using IMS.Domain.Entities.Order;
using IMS.Domain.Repositories.Interfaces;
using MongoDB.Driver;

namespace IMS.Domain.Repositories;

public class OrderRepository : IOrderRepository
{
    private readonly IRepository<Order> _repository;
    private readonly IWorkContext _workContext;
    public OrderRepository(IWorkContext workContext,IRepository<Order> repository)
    {
        _workContext = workContext;
        _repository = repository;
    }

    public async Task<Order> GetByIdAsync(string id)
    {
        var result = await _repository.GetByIdAsync(id);
        if (result == null) throw Exceptions.CreateNotFoundException(nameof(Order));;
        return result;
    }

    public async Task<Order> InsertAsync(Order order, IClientSessionHandle? sessionHandle = null)
    {
        order.InitializeInsert(_workContext.WorkContext.UserId, DateTime.UtcNow);
        return await _repository.InsertAsync(order, sessionHandle);
    }

    public async Task<Order> UpdateAsync(Order order, IClientSessionHandle? sessionHandle = null)
    {
        order.InitializeUpdate(_workContext.WorkContext.UserId, DateTime.UtcNow);
        return await _repository.UpdateAsync(order, sessionHandle);
    }

    public IQueryable<Order> Table => _repository.Table;
}//No need for update or delete. once created then that's it.
