using FalconFlexCommon.Mongo.Repositories;
using FalconFlexCommon.UserContext;
using IMS.Domain.Entities.Product;
using IMS.Domain.Repositories.Interfaces;
using MongoDB.Driver;

namespace IMS.Domain.Repositories;

public class ProductRepository : IProductRepository
{
    private readonly IRepository<Product> _repository;
    private readonly IWorkContext _workContext;

    public ProductRepository(IWorkContext workContext, IRepository<Product> repository)
    {
        _workContext = workContext;
        _repository = repository;
    }

    public async Task<Product> GetByIdAsync(string id)
    {
        var result = await _repository.GetByIdAsync(id);
        return result;
    }

    public async Task<Product> InsertAsync(Product product, IClientSessionHandle? sessionHandle = null)
    {
        product.InitializeInsert(_workContext.WorkContext.UserId, DateTime.UtcNow);
        return await _repository.InsertAsync(product, sessionHandle);
    }

    public async Task<Product> UpdateAsync(Product product, IClientSessionHandle? sessionHandle = null)
    {
        product.InitializeUpdate(_workContext.WorkContext.UserId, DateTime.UtcNow);
        return await _repository.UpdateAsync(product, sessionHandle);
    }

    public async Task<Product> DeleteAsync(Product product, IClientSessionHandle? sessionHandle = null)
    {
        return await _repository.DeleteAsync(product, sessionHandle);
    }

    public IQueryable<Product> Table => _repository.Table;
}