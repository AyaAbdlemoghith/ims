using IMS.Domain.Entities.Order;
using MongoDB.Driver;

namespace IMS.Domain.Repositories.Interfaces;

public interface IOrderRepository
{
    IQueryable<Order> Table { get; }
    Task<Order> GetByIdAsync(string id);
    Task<Order> UpdateAsync(Order order, IClientSessionHandle? sessionHandle = null);
    Task<Order> InsertAsync(Order order, IClientSessionHandle? sessionHandle = null);

}