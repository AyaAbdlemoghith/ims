using IMS.Domain.Entities.Shipment;
using MongoDB.Driver;

namespace IMS.Domain.Repositories.Interfaces;

public interface IShipmentRepository
{
    IQueryable<Shipment> Table { get; }
    Task<Shipment> GetByIdAsync(string id);
    Task<Shipment> InsertAsync(Shipment shipment, IClientSessionHandle? sessionHandle = null);

}