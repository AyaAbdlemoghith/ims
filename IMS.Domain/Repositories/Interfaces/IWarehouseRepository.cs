using IMS.Domain.Entities;
using MongoDB.Driver;

namespace IMS.Domain.Repositories.Interfaces;

public interface IWarehouseRepository
{
    IQueryable<Warehouse> Table { get; }
    Task<Warehouse> GetByIdAsync(string id);
    Task<Warehouse> InsertAsync(Warehouse warehouse, IClientSessionHandle? sessionHandle = null);
    Task<Warehouse> UpdateAsync(Warehouse warehouse, IClientSessionHandle? sessionHandle = null);

}