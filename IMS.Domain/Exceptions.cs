namespace IMS.Domain
{
    public static class Exceptions
    {
        #region NotFoundException
        public class NotFoundException : Exception
        {
            public NotFoundException(string message) : base(message)
            {
            }
        }

        public static NotFoundException CreateNotFoundException(string entityName)
        {
            return new NotFoundException($"'{entityName}' not found with given id.");
        }
        #endregion

        #region NullOrEmptyException
        public class NullOrEmptyException : Exception
        {
            public NullOrEmptyException(string parameterName) : base($"'{parameterName}' is null or empty.")
            {
            }
        }

        public static NullOrEmptyException CreateNullOrEmptyException(string parameterName)
        {
            return new NullOrEmptyException(parameterName);
        }
        #endregion
        
        #region DateInvalidException
        public class DateInvalidException : Exception
        {
            public DateInvalidException(string parameterName) : base($"'{parameterName}' contains an invalid date.")
            {
            }
        }

        public static DateInvalidException CreateDateInvalidException(string parameterName)
        {
            return new DateInvalidException(parameterName);
        }
        #endregion
        
        #region QuantityInvalidException
        public class ZeroOrNegNumInvalidException : Exception
        {
            public ZeroOrNegNumInvalidException(string parameterName) : base($"'{parameterName}' must be 1 or above.")
            {
            }
        }

        public static ZeroOrNegNumInvalidException CreateZeroOrNegNumInvalidException(string parameterName)
        {
            return new ZeroOrNegNumInvalidException(parameterName);
        }
        #endregion

    }

    /*
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                // Simulating entity not found scenario
                throw Exceptions.CreateNotFoundException("Product");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
    */
}