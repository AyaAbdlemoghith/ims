using Ardalis.SmartEnum;
using IMS.Domain.Constants;

namespace IMS.Domain.Enums
{
    public abstract class OrderStatus : SmartEnum<OrderStatus, int>
    {
        public static readonly OrderStatus NewOrder = new NewOrderStatus();
        public static readonly OrderStatus IsDeliveringOrder = new IsDeliveringOrderStatus();
        public static readonly OrderStatus DeliveredOrder = new DeliveredOrderStatus();
        public static readonly OrderStatus CancelledOrder = new CancelledOrderStatus();

        private OrderStatus(string name, int value) : base(name, value)
        {
        }

        public abstract bool IsTransitionToNextStatus(int next);
        public abstract bool IsDecreasingStock();
        public abstract bool IsIncreasingStock();



        private sealed class NewOrderStatus : OrderStatus
        {
            public NewOrderStatus() : base(nameof(OrderStatusConstants.New).ToLowerInvariant(), OrderStatusConstants.NewValue)
            {
            }
            public override bool IsTransitionToNextStatus(int next)
            {
                 //return true;//allow for only Inprog and cancell
                 return (next == IsDeliveringOrder.Value) || (next == CancelledOrder.Value);

            }
            public override bool IsDecreasingStock()
            {
                 return true;   
            }
            public override bool IsIncreasingStock()
            {
                 return false;   
            }
        }
        private sealed class IsDeliveringOrderStatus : OrderStatus
        {
            public IsDeliveringOrderStatus() : base(nameof(OrderStatusConstants.IsDelivering).ToLowerInvariant(), OrderStatusConstants.IsDeliveringValue)
            {
            }
            public override bool IsTransitionToNextStatus(int next)//do nothing to stock
            {
                return next == DeliveredOrder.Value;

            }
            public override bool IsDecreasingStock()
            {
                return false;   
            }
            public override bool IsIncreasingStock()
            {
                return false;   
            }
        }
        private sealed class DeliveredOrderStatus : OrderStatus
        {
            public DeliveredOrderStatus() : base(nameof(OrderStatusConstants.Delivered).ToLowerInvariant(), OrderStatusConstants.DeliveredValue)
            {
            }
            public override bool IsTransitionToNextStatus(int next)
            {
                 return false;
            }
            public override bool IsDecreasingStock()
            {
                 return false;   
            }
            public override bool IsIncreasingStock()
            {
                return false;   
            }
        }

        private sealed class CancelledOrderStatus : OrderStatus
        {
            public CancelledOrderStatus() : base(nameof(OrderStatusConstants.Cancelled).ToLowerInvariant(), OrderStatusConstants.CancelledValue)
            {
            }
            public override bool IsTransitionToNextStatus(int next)
            {
                 return false;
            }
              public override bool IsDecreasingStock()
            {
                 return false;   
            }
            public override bool IsIncreasingStock()
            {
                return true;   
            }
        }
    }
}
