using Ardalis.SmartEnum;
using IMS.Domain.Constants;

namespace IMS.Domain.Enums;

public abstract class Category : SmartEnum<Category>//, IComparable
{
    public static Category Grains = new GrainsCategory ();
    public static Category Dairy = new DairyCategory ();
    public static Category Beverages = new BeveragesCategory ();
    public static Category Snacks = new SnacksCategory ();
    public static Category Fruits = new FruitsCategory ();
    public static Category Pharmaceuticals = new PharmaceuticalsCategory ();
    public static Category Clothing = new ClothingCategory ();
    public static Category Electronics = new ElectronicsCategory ();
    public static Category Furniture = new FurnitureCategory ();
    public static Category Chemicals = new ChemicalsCategory ();
    private Category(string name, int value) : base(name, value)
    {
    }

    public abstract bool IsExpiryDateNeeded();
    public abstract bool IsReturnAllowed();


    private sealed class GrainsCategory : Category
    {
        public GrainsCategory() : base(nameof(CategoryConstants.Grains).ToLowerInvariant(),CategoryConstants.GrainsValue)
        {
        }
        public override bool IsExpiryDateNeeded()
        {
            return true;
        }  
        public override bool IsReturnAllowed()
        {
            return false;
        }  
    }
    private sealed class DairyCategory : Category
    {
        public DairyCategory() : base(nameof(CategoryConstants.Dairy).ToLowerInvariant(),CategoryConstants.DairyValue)
        {
        }
        
        public override bool IsExpiryDateNeeded()
        {
            return true;
        }  
        public override bool IsReturnAllowed()
        {
            return false;
        }  
    }
    private sealed class BeveragesCategory : Category
    {
        public BeveragesCategory() : base(nameof(CategoryConstants.Beverages).ToLowerInvariant(),CategoryConstants.BeveragesValue)
        {
        }
        public override bool IsExpiryDateNeeded()
        {
            return true;
        }  
        public override bool IsReturnAllowed()
        {
            return false;
        }  
    }
    private sealed class SnacksCategory : Category
    {
        public SnacksCategory() : base(nameof(CategoryConstants.Snacks).ToLowerInvariant(),CategoryConstants.SnacksValue)
        {
        }
        public override bool IsExpiryDateNeeded()
        {
            return true;
        }  
        public override bool IsReturnAllowed()
        {
            return false;
        }  
    }
    private sealed class FruitsCategory : Category
    {
        public FruitsCategory() : base(nameof(CategoryConstants.Fruits).ToLowerInvariant(),CategoryConstants.FruitsValue)
        {
        }
        
        public override bool IsExpiryDateNeeded()
        {
            return true;
        }  
        public override bool IsReturnAllowed()
        {
            return false;
        }  
    }
    private sealed class PharmaceuticalsCategory : Category
    {
        public PharmaceuticalsCategory() : base(nameof(CategoryConstants.Pharmaceuticals).ToLowerInvariant(),CategoryConstants.PharmaceuticalsValue)
        {
        }
        public override bool IsExpiryDateNeeded()
        {
            return true;
        }  
        public override bool IsReturnAllowed()
        {
            return false;
        }  
    }
    private sealed class ClothingCategory : Category
    {
        public ClothingCategory() : base(nameof(CategoryConstants.Clothing).ToLowerInvariant(),CategoryConstants.ClothingValue)
        {
        }
        
        public override bool IsExpiryDateNeeded()
        {
            return false;
        }  
        public override bool IsReturnAllowed()
        {
            return true;
        }  
    }
    private sealed class ElectronicsCategory : Category
    {
        public ElectronicsCategory() : base(nameof(CategoryConstants.Electronics).ToLowerInvariant(),CategoryConstants.ElectronicsValue)
        {
        }
        public override bool IsExpiryDateNeeded()
        {
            return false;
        }  
        public override bool IsReturnAllowed()
        {
            return true;
        }  
    }
    private sealed class FurnitureCategory : Category
    {
        public FurnitureCategory() : base(nameof(CategoryConstants.Furniture).ToLowerInvariant(),CategoryConstants.FurnitureValue)
        {
        }
        
        public override bool IsExpiryDateNeeded()
        {
            return false;
        }  
        public override bool IsReturnAllowed()
        {
            return true;
        }  
    }
    private sealed class ChemicalsCategory : Category
    {
        public ChemicalsCategory() : base(nameof(CategoryConstants.Chemicals).ToLowerInvariant(),CategoryConstants.ChemicalsValue)
        {
        }
        
        public override bool IsExpiryDateNeeded()
        {
            return false;
        }  
        public override bool IsReturnAllowed()
        {
            return true;
        }  
    }
 
}