using CSharpFunctionalExtensions;
using IMS.Domain.Enums;
using Ardalis.SmartEnum.JsonNet;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
namespace IMS.Domain.Entities.Shipment;

public class ShipmentItem: ValueObject
{

    public ShipmentItem(string prodId, int shipmentQuantity)
    {
        SetProdId( prodId );
        SetShipmentQuantity( shipmentQuantity );
    }
    
    public string ProdId { get; private set; }
    public int ShipmentQuantity { get;  private set; }
    private void SetShipmentQuantity(int shipmentQuantity)
    {
        if (shipmentQuantity < 1) throw Exceptions.CreateZeroOrNegNumInvalidException("Shipment Quantity");
        ShipmentQuantity = shipmentQuantity;
    }
    private void SetProdId(string prodId)
    {
        if (string.IsNullOrEmpty(prodId)) throw Exceptions.CreateNullOrEmptyException(nameof(Product));
        ProdId = prodId;
    }
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return ProdId;
        yield return ShipmentQuantity;
    }

     

}