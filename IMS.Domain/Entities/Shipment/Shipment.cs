using FalconFlexCommon.Mongo.BaseClasses;

namespace IMS.Domain.Entities.Shipment;

public class Shipment :BaseEntity
{
   public Shipment(DateTime shipmentDate, string warehouseId, List<ShipmentItem> shipmentItems)
    {
        SetShipmentDate( shipmentDate);
        SetWarehouseId( warehouseId);
        SetShipmentItems( shipmentItems);
    }
    public DateTime ShipmentDate { get; private set;}
    public string WarehouseId { get; private set; }
    public List<ShipmentItem> ShipmentItems { get; set; } 
    private void SetShipmentDate(DateTime shipmentDate)
    {
         ShipmentDate = shipmentDate;
    }
    private void SetWarehouseId(string warehouseId)
    {
        if (string.IsNullOrEmpty(warehouseId)) throw Exceptions.CreateNullOrEmptyException(nameof(Warehouse));
        WarehouseId = warehouseId;
    }
    private void SetShipmentItems(List<ShipmentItem> shipmentItems)
    {
        ShipmentItems = shipmentItems;
    }
    
}

