using FalconFlexCommon.Mongo.BaseClasses;

namespace IMS.Domain.Entities;

public class Warehouse :BaseEntity
{
    public string WarehouseName { get; private set; }
    public string WarehouseLocation { get; private set; }

    public Warehouse(string warehouseName, string warehouseLocation)
    {
        SetWarehouseName( warehouseName );
        SetWarehouseLocation( warehouseLocation );
    }
    public Warehouse Update(string? warehouseName = null, string? warehouseLocation = null)
    {
        if(warehouseName is not null)
            SetWarehouseName( warehouseName );
        if(warehouseLocation is not null)
            SetWarehouseLocation( warehouseLocation );
        return this;
    }
    private void SetWarehouseName(string warehouseName)
    {
        if (string.IsNullOrEmpty(warehouseName)) throw Exceptions.CreateNullOrEmptyException(nameof(Warehouse));
        WarehouseName = warehouseName;
    }
    private void SetWarehouseLocation(string warehouseLocation)
    {
        if (string.IsNullOrEmpty(warehouseLocation)) throw Exceptions.CreateNullOrEmptyException("Warehouse Location");
        WarehouseLocation = warehouseLocation;
    }
}