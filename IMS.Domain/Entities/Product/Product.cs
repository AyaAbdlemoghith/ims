using Ardalis.SmartEnum.JsonNet;
using FalconFlexCommon.Mongo.BaseClasses;
using IMS.Domain.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
 
namespace IMS.Domain.Entities.Product;

public class Product:BaseEntity
{
    private const int TwoWeeks = 14;

    public Product(string productName,decimal productPrice,int categoryId,DateTime? productExpiryDate, List<ProductItem> productItems)
    {
        SetProductName(productName);
        SetProductPrice(productPrice); 
        SetCategoryId(categoryId);
        SetProductExpiryDate(productExpiryDate); 
        SetProductItems(productItems);
    }

    public void SetCategoryId(int categoryId)
    {
        CategoryId = categoryId;
    }

    public List<ProductItem> ProductItems { get; set; }
    private void SetProductItems(List<ProductItem> productItems)
    {
        ProductItems = productItems;
    }
    public string ProductName { get; private set; }
    private void SetProductName(string productName)
    {
        if (string.IsNullOrEmpty(productName)) throw Exceptions.CreateNullOrEmptyException(nameof(Product));
        ProductName = productName;
    }
    public Product Update(string? productName = null,decimal? productPrice=null, int? categoryId=null, DateTime? productExpiryDate= null, List<ProductItem>?productItems=null)//now here it's saying both are null what if he changes only the expiry date 
    {
        if (productName is not null)
            SetProductName(productName);
        if (productPrice is not null)
            SetProductPrice(productPrice);
        if (categoryId is not null)//if (productCategory is null) then don't set the productExDate; else set it
        {            
            SetCategoryId(categoryId);
            if (productExpiryDate is not null) SetProductExpiryDate(productExpiryDate);
            //I want to make sure that if the ProdCat didn't change then expiry don't change either.
        }
        else
        {
            if (productExpiryDate is not null)
                throw new Exception("Cannot change the expiry date of a product if the Category didn't change");
        }
        if (productItems is not null)
            SetProductItems(productItems);
        return this;
        //chatgpt
    }

    private void SetCategoryId(int? categoryId)
    {
        //CategoryId != null ? Category.FromValue(CategoryId.Value) : null;

        if (categoryId != null) CategoryId = (int)categoryId;
    }
    private void SetProductPrice(decimal? productPrice)
    {
        if (productPrice ==null) throw Exceptions.CreateNullOrEmptyException("Product Price");
        if (productPrice < 1) throw Exceptions.CreateZeroOrNegNumInvalidException("Product Price");
         ProductPrice = productPrice;    
    }
    private void EnsureValid()
    {
        throw new NotImplementedException();
    }
    public decimal? ProductPrice { get; private set; }
    public DateTime? ProductExpiryDate { get; protected internal set; }
    public int CategoryId { get; set; }

    [BsonIgnore]
    [JsonConverter(typeof(SmartEnumNameConverter<Category, int>))]
    public Category ProductCategory
    {
        get => Category.FromValue(CategoryId);
        set => CategoryId = value.Value;
    }

    private void SetProductExpiryDate(DateTime? productExpiryDate)
    {
        if (ProductCategory.IsExpiryDateNeeded())
        {
            ProductExpiryDate = productExpiryDate ?? throw Exceptions.CreateDateInvalidException(nameof(ProductName));
            DateTime today = DateTime.Now.Date;
            TimeSpan? difference = productExpiryDate.Value.Date - today;// Accessing the value when it's not null

            if (difference?.TotalDays < TwoWeeks)
                throw new NullReferenceException("Product expiry date has less than 2 weeks away or expired.");
        }

        else ProductExpiryDate = null;
    }

    private void SetProductCategory(Category? productCategory)
    {
        if (productCategory == null) throw new ArgumentException("Null ");
        ProductCategory = productCategory;    
    }


}
