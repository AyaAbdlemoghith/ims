using CSharpFunctionalExtensions;

namespace IMS.Domain.Entities.Product;

public class ProductItem:ValueObject
{
    public ProductItem(string warehouseId, int prodQuantity)
    {
        SetWarehouseId(warehouseId);
        SetProdQuantity(prodQuantity);
    }
    public string WarehouseId { get; set; }
    public int ProdQuantity { get; set; }

    private void SetProdQuantity(int prodQuantity)
    {
        if (prodQuantity < 1) throw Exceptions.CreateZeroOrNegNumInvalidException("Product Quantity");
        ProdQuantity = prodQuantity;    
    }

    private void SetWarehouseId(string warehouseId)
    {
        if (string.IsNullOrEmpty(warehouseId)) throw Exceptions.CreateNullOrEmptyException(nameof(Warehouse.WarehouseName));
        WarehouseId = warehouseId;
    }
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return WarehouseId;
        yield return ProdQuantity;
    }
    
}