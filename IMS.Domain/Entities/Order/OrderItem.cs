using CSharpFunctionalExtensions;

namespace IMS.Domain.Entities.Order;

public class OrderItem:ValueObject
{
   
    public OrderItem(string prodId, int orderQuantity)
     {
         SetProdId( prodId );
         SetOrderQuantity( orderQuantity );
     }
    public string ProdId { get; private set; }
    public int OrderQuantity { get; private set; }
    private void SetOrderQuantity(int orderQuantity)
    {
        if (orderQuantity < 1) throw Exceptions.CreateZeroOrNegNumInvalidException("Order Quantity");
        OrderQuantity = orderQuantity;
    }

    private void SetProdId(string prodId)
    {
        if (string.IsNullOrEmpty(prodId)) throw Exceptions.CreateNullOrEmptyException(nameof(Product.Product.ProductName));

        ProdId = prodId;
    }

 
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return ProdId;
        yield return OrderQuantity;
    }
}