using Ardalis.SmartEnum.JsonNet;
using FalconFlexCommon.Mongo.BaseClasses;
using IMS.Domain.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace IMS.Domain.Entities.Order;

public class Order:BaseEntity
{

    public Order(DateTime orderDate, string warehouseId, List<OrderItem> orderItems, int orderStatusId)
    {
        
        SetOrderDate(orderDate);
        SetWarehouseId(warehouseId);
        SetOrderItems(orderItems);
        SetOrderStatusId(orderStatusId);
    }

    private void SetOrderStatusId(int orderStatusId)
    {
        OrderStatusId= orderStatusId;
    }

    public DateTime OrderDate { get; private set; }
    public string WarehouseId { get; private set; }
    public List<OrderItem> OrderItems { get; private set; }
    public int OrderStatusId { get; set; }
    [BsonIgnore]
    [JsonConverter(typeof(SmartEnumNameConverter<OrderStatus, int>))]
    public OrderStatus OrderStatus {
        get => OrderStatus.FromValue(OrderStatusId);
        set => OrderStatusId = value.Value;
    }
    
    private void SetOrderStatus(OrderStatus orderStatus)
    {
        OrderStatus = orderStatus;
    }
    private void SetOrderItems(List<OrderItem> orderItems)
    {
        OrderItems = orderItems;
    }
    private void SetWarehouseId(string warehouseId)
    {
        if (string.IsNullOrEmpty(warehouseId)) throw Exceptions.CreateNullOrEmptyException(nameof(Warehouse));
        WarehouseId = warehouseId;
    }
    private void SetOrderDate(DateTime orderDate)
    {
        //if (orderDate != DateTime.MinValue) throw Exceptions.CreateDateInvalidException(nameof(Order));
        OrderDate = orderDate;
    }

 
}
