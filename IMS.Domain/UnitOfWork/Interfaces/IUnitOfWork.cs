using IMS.Domain.Repositories.Interfaces;
using MongoDB.Driver;

namespace IMS.Domain.UnitOfWork.Interfaces;

public interface IUnitOfWork
{
    IOrderRepository OrderRepository { get; }
    IProductRepository ProductRepository { get; }
    IShipmentRepository ShipmentRepository { get; }
    IWarehouseRepository WarehouseRepository { get; }

    public Task RunInTransaction(Func<IClientSessionHandle, Task> transactionMethod);
}