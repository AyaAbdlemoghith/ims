using FalconFlexCommon.Mongo.DbContext;
using FalconFlexCommon.Mongo.Repositories;
using FalconFlexCommon.UserContext;
using IMS.Domain.Entities;
using IMS.Domain.Entities.Order;
using IMS.Domain.Entities.Product;
using IMS.Domain.Entities.Shipment;
using IMS.Domain.Repositories;
using IMS.Domain.Repositories.Interfaces;
using IMS.Domain.UnitOfWork.Interfaces;
using MongoDB.Driver;

namespace IMS.Domain.UnitOfWork;

public class UnitOfWork : IUnitOfWork
{
    private readonly IMongoDatabase _database;
    private readonly IDatabaseContext _databaseContext;
    private IProductRepository _productRepository;
    private IOrderRepository _orderRepository;
    private IWarehouseRepository _warehouseRepository;
    private IShipmentRepository _shipmentRepository;
    private readonly IWorkContext _workContext;

    public UnitOfWork(IMongoDatabase database, IWorkContext workContext, IDatabaseContext databaseContext)
    {
        _database = database;
        _workContext = workContext;
        _databaseContext = databaseContext;
    }


    public IProductRepository ProductRepository
    {
        get { return _productRepository ??= new ProductRepository(_workContext, new Repository<Product>(_database)); }
    }

    public IWarehouseRepository WarehouseRepository
    {
        get { return _warehouseRepository ??= new WarehouseRepository(_workContext,new Repository<Warehouse>(_database)); }
    }
    
    public IOrderRepository OrderRepository
    {
        get
        {
            return _orderRepository ??=
                new OrderRepository(_workContext,new Repository<Order>(_database));
        }
    }

    public IShipmentRepository ShipmentRepository
    {
        get
        {
            return _shipmentRepository ??= new ShipmentRepository(_workContext,new Repository<Shipment>(_database));
        }
    }

    public async Task RunInTransaction(Func<IClientSessionHandle, Task> transactionMethod)
    {
        await _databaseContext.RunInTransaction(transactionMethod);
    }
}